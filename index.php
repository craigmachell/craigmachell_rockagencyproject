<?php get_header(); ?>

<!--        Header Section-->

<div class="container-fluid">
    
    <img id="header-img" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1502110646/header-image_fctc1e.jpg" alt="light-blue-background">
                
    <img id="logo-header" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1502109562/logo_dmpmkg.png" alt="white-logo-clear-writing">
    
</div>
    
        
<!--        Section 1 - Introduction-->
<section class="section-margin">

    <div id="section-1">
    
        <h1 id="title">Lorem Ipsum Dolor</h1>
        <br>
        <h3 id="sub-heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae blandit libero, euismod rhoncus elit. Nulla scelerisque ante felis, in pellentesque enim ultricies venenatis. Etiam hendrerit eleifend suscipit</h3>
        
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae blandit libero, euismod rhoncus elit. Nulla scelerisque ante felis, in pellentesque enim ultricies venenatis.</p>
        
        <p>Etiam hendrerit eleifend suscipit. Aenean tincidunt elementum porta. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc bibendum consectetur tellus, in pellentesque enim ultricies venenatis.</p>
        
        <button class="btn" id="btn-red">learn more</button>
        
    </div>
    
</section>

<!--        Section 2 - Red Banner-->
<div class="container-fluid" id="banner-red">
    
    <div id="banner-text">In turpis mauris, cursus a nisi sit amet, ultrices facilisis mauris. Morbi in eros condimentum sapien molestie vehicula.
    </div>

</div>

<!--        Section 3 - Get Ready..-->
<section class="section-margin">

<div class="container" id="section-3">

    <h2>Get ready for glowing skin, vibrant health and renewed energy!</h2>
        
    <div class="row">
    
            <div class="col-sm-6">
            
                <h4>Lorem Ipsum</h4>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae blandit libero, euismod rhoncus elit. Nulla scelerisque ante felis, in pellentesque enim</p>
                
                <p>Ultricies venenatis. Etiam hendrerit eleifend suscipit. Aenean tincidunt elementum porta. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc bibendum consectetur tellus, sed pretium dolor mollis sit amet. Maecenas suscipit tortor euismod porttitor laoreet. Ut aliquam posuere ante. Fusce sed nulla ut neque molestie tincidunt. In viverra, est auctor</p>
                
                <h3 class="bold-italic">Lorem Ipsum</h3>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae blandit libero, euismod rhoncus elit. Nulla scelerisque.</p>
                
            </div>
                        
            <div class="col-sm-6">
            
                <h4>Lorem Ipsum</h4>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae blandit libero, euismod rhoncus elit. Nulla scelerisque ante felis etiam hendrerit eleifend</p>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing. ‘Aliquam! vitae blandit libero elit!’</p>
                
                <p>ultricies venenatis. Etiam hendrerit eleifend suscipit. Aenean tincidunt elementum porta. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc bibendum consectetur tellus, sed pretium dolor mollis sit amet. Maecenas suscipit tortor euismod</p>
                
                <p>porttitor laoreet. Ut aliquam posuere ante. Fusce sed nulla ut neque molestie tincidunt. In viverra, est auctor fringilla pharetra, sem nisl maximus diam, in laoreet orci tortor a mi.</p>
                
            </div>
            </div>
    </div>
</section>
    
<!--        Section 4 - Gallery Banner-->

<div class="container-fluid" id="gallery-banner">

    <div class="gallery-div">
       
        <img class="gallery-img" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1501839032/image6_m9tmay.jpg" alt="hot-air-balloons">
    
    </div>
    
    <div class="gallery-div">
    
        <img class="gallery-img" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1501839030/image4_apr2n3.jpg" alt="spiral-architecture">
    
    </div>
    
    <div class="gallery-div">
    
        <img class="gallery-img" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1501839033/image5_edxn6b.jpg" alt="couple-hiking">
    
    </div>
    
    <div class="gallery-div">
    
        <img class="gallery-img" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1501839033/image7_u5tuua.jpg" alt="slackline-at-sunset">
    
    </div>

</div>

<!--        Section 5 - Animal Articles-->

<section class="section-margin">

    <div class="container" id="section-5">
        
        <h2 id="section-5-title">Lorem ipsum dolor sit</h2>
    
        <div class="row">
        
            <div class="col-sm-5">
            
                <img class= "img-animals" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1501839030/image1_lnob8d.jpg" alt="tiger-in-water">
                
                <h4>Lorem Ipsum</h4>
                
                <p>Euismod rhoncus elit. Nulla scelerisque ante felis, in pellentesque enim ultricies venenatis. Etiam hendrerit eleifend suscipit. Aenean tincidunt elementum porta. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc bibendum consectetur tellus, sed pretium dolor mollis sit amet. Maecenas suscipit tortor euismod porttitor laoreet. Ut aliquam posuere ante.</p>
                
                <button class="btn btn-animals">find out more</button>
                
            </div>
            
            <div class="col-sm-2"></div>
            
            <div class="col-sm-5">
            
                <img class="img-animals" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1501839030/image2_pqx0ar.jpg" alt="elephant-in-sand">
                
                <h4>Dolor Amet</h4>
                
                <p>Euismod rhoncus elit. Nulla scelerisque ante felis, in pellentesque enim ultricies venenatis. Etiam hendrerit eleifend suscipit. Aenean tincidunt elementum porta. Interdum et malesuada fames ac ante ipsum primis in faucibus. Nunc bibendum consectetur tellus, sed pretium dolor mollis sit amet. Maecenas suscipit tortor euismod porttitor laoreet. Ut aliquam posuere ante.</p>
                
                <button class="btn btn-animals">find out more</button>
            
            </div>
        
        </div>
    
    </div>

</section>

<!--        Section 6 - Fruit Section/aqua background-->

<div class="container-fluid" id="background-aqua">
    
    <div class="container" id="section-6">
    
        <h2 id="fruit-title">Lorem ipsum dolor</h2>
        
        <h3 class="bold-italic" id="fruit-subheading">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ipsum nisi, tincidunt condimentum sapien in, porta fermentum</h3>
        
        <div class="post-container">
        
            <div class="post-thumb">
            
                <img class="img-fruits" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1501839034/image9_fs4xig.jpg" alt="bananas">
                
            </div>
            
            <div class="post-content">
                
                <h3 class="bold-italic">Lorem Ipsum</h3>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae blandit libero, euismod rhoncus elit.</p>
                
                <button class="btn btn-fruits">learn more about banana</button>
                
            </div>
            
        </div>
        
        <div class="post-container">
        
            <div class="post-thumb">
            
                <img class="img-fruits" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1501839034/image8_enplxj.jpg" alt="apples">
                
            </div>
            
            <div class="post-content">
                
                <h3 class="bold-italic">Dolor Sit</h3>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam vitae blandit libero, euismod rhoncus elit. Lorem ipsum dolor sit amet.</p>
                
                <button class="btn btn-fruits">learn more about apple</button>
                
            </div>
            
        </div>
        
        <div class="post-container" id="btm-container">
        
            <div class="post-thumb">
            
                <img class="img-fruits" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1501839030/image3_mkeshb.jpg" alt="strawberries">
                
            </div>
            
            <div class="post-content">
                
                <h3 class="bold-italic">Blandit Libero</h3>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit aliquam vitae.</p>
                
                <button class="btn btn-fruits">learn more about strawberry</button>
                
            </div>
            
        </div>
    
        </div>
        
    </div>

<!--            Section 7 - Bottom Section Red Banner-->
            
<div class="container-fluid" id="btm-section">
            
    <img id="btm-logo" src="http://res.cloudinary.com/dmwdnrpzy/image/upload/v1502109562/logo_dmpmkg.png" alt="white-logo-clear-writing">
    
    <h2 id="btm-title">Lorem ipsum dolor sit amet, consectetur adipiscing
    </h2>
    
    <button class="btn">find out more</button>
    
</div> 

<?php get_footer(); ?>  