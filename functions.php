<?php
/*
    ========================================
    Include scripts
    ========================================
*/
function craig_rockagency_script_enqueue() {
	
    //css
	wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '4.0.0', 'all');
	wp_enqueue_style('customstyle', get_template_directory_uri() . '/css/craig_rockagency.css', array(), '1.0.0', 'all');
	
    //js
	wp_enqueue_script('jquery');
	wp_enqueue_script('bootstrapjs', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '4.0.0', true);
	wp_enqueue_script('customjs', get_template_directory_uri() . '/js/craig_rockagency.js', array(), '1.0.0', true);
	
}

add_action( 'wp_enqueue_scripts', 'craig_rockagency_script_enqueue');

/*
    ========================================
    Theme support functions
    ========================================
//*/
//add_theme_support('custom-background');
//add_theme_support('custom-header');

