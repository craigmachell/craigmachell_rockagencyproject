<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Rock Agency Project</title>
  <?php wp_head(); ?>
  <style>
      
    html,
    body {
      margin: 0 auto;
      font-family: 'Helvetica Neue', Arial, sans-serif;
      font-size: 16px;
      text-align: center; 
      }

    h1,
    h2,
    h4 {
      font-family: AchilleFYW01-Black;
    }
    
      .container-fluid {
          padding: 0;
      }
      
    #header-img {
      height: 350px;
      width: 100%;
    }

    #logo-header {
      height: 160px;
      width: 160px;
      position: inherit;
      bottom: 250px;
      right: 0px;
      margin: 0 auto;
    }

    .section-margin {
      margin-top: 50px;
      margin-bottom: 50px;
    }

    #section-1 {
      width: 62%;
      margin: 0 auto; 
      margin-top: -140px;
    }

    #title {
      color: #E55049;
      font-family: AchilleFYW01-Black;
      font-size: 60px;
      margin-bottom: 20px;
      margin: 0 auto;
    }

    #sub-heading {
      font-style: italic;
      font-size: 19px;
      font-weight: 500;
    }

    .btn {
      padding: 14px 50px;
      border: 0 none;
      font-weight: 500;
      letter-spacing: 1px;
      text-transform: uppercase;
      background: #9FCECC;
      pointer-events: none;
      margin: 0 auto;
      height: 40px;
      line-height: 1px;
      color: white;
      font-weight: bold;
      margin-top: 20px;
    }

    #btn-red {
      background: #E55049;
      padding: 15px 35px;
    }

    #banner-red {
      background: #E55049;
      width: 100%;
      padding-bottom: 30px;
      padding-top: 30px;
    }

    #banner-text {
      color: white;
      font-size: 22px;
      font-style: italic;
      font-weight: bold;
      margin: 0 auto;
      width: 62%;
    }

    #section-3 {
      margin-top: 50px;
      width: 70%;
    }

    h2 {
      color: #E55049;
      font-size: 40px;
      margin-bottom: 50px;
    }

    h4 {
      color: #9FCECC;
      font-size: 28px;
      margin-bottom: 20px;
    }

    #gallery-banner {
      margin-left: -20px;
    }

    #gallery-div {
        width: 100%;
    }

    .gallery-img{
        height: 317px;
        width: 317px; 
        padding: 0;
        float: left;
    }
      
    @media(max-width:768px) {
      .gallery-img {
         width: 25%; 
         float: left;
       }
        
    }      
      
    #section-5 {
      width: 70%;
      margin-top: 420px;
    }        

    .img-animals {
      border-radius: 50%;
      height: 180px;
      margin-bottom: 30px;
      margin-top: 50px;
    }
      
    @media(max-width:768px) {
      .img-animals {
        margin-top: 30px;
      }
        
      .btn-animals {
        width: wrap ;
        height: wrap; 
      }
    }      

    .bold-italic {
      font-style: italic;
      font-weight: bold;
      font-size: 21px;
    }

    #background-aqua {
      background: #e8fcfa;
      padding-top: 40px;
      padding-bottom: 40px;
    }
      
    #section-5 h2 {
      margin-bottom: -20px;
    }  

    #section-6 {
      width: 70%;
      text-align: left;
    }

    #fruit-title {
      margin-bottom: 20px;
      text-align: center;
    }

    #fruit-subheading {
      margin-bottom: 40px;
      text-align: center;
    }

    .post-container {
      padding-top: 10px;
      background: white;
      border-radius: 2%;
      width: 100%;
      margin: 15px;
      height: 165px;
    }

    .btn-fruits {
      height: 20px;
      padding: 15px;
      margin-bottom: 25px;
      margin-top: 5px;
    }

    .img-fruits {
      height: 140px;
      border-radius: 50%;
      margin: 2px 15px 10px 15px;
    }

    .post-thumb {
      float: left;
    }

    .post-content {
      margin-right: 40px;
    }
        
    @media(max-width:768px) {
      .img-fruits {
        height: 80px;
      }
        
      .post-container {
        width: 100%;
        height: 310px;;
        font-size: 14px;
      }
        
      .btn-fruits {
        margin-top: -10px;
        font-size: 8px;
        height: 25px;
        width: 185px;
        padding-left: 10px;
        padding-right: 10px;
      }
        
      #btm-container {
        height: 270px;
      }
    }
         
    #btm-section {
      background: #E55049;
      height: 400px;
      padding-top: 40px;
    }

    #btm-logo {
      height: 130px;
      width: 130px;
      margin-bottom: 30px;
    }

    #btm-title {
      color: white;
      width: 60%;
      margin: 0 auto;
    }
      
    @media(max-width:768px) {
      #btm-section {
        height: 550px;
      } 
        
      #btm-title {
        font-size: 30px;
      }
    }

    footer {
      color: #9FCECC;
      padding-top: 20px;
      margin-bottom: 10px;
      font-size: 12px;
    }

    a {
      color: inherit;
      text-decoration: underline;
    }
      
  </style>
</head>